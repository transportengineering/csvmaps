{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE ExplicitNamespaces  #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators       #-}
module Main where
import           Control.Lens                     (failing, iforM_, re, to,
                                                   (^?), _Left, _Right)
import           Control.Monad                    (when)
import           Control.Monad.Trans.Resource     (MonadResource, runResourceT)
import           Control.Monad.Trans.State.Strict (State, get, modify',
                                                   runState)
import           Data.ByteString                  (ByteString)
import qualified Data.ByteString.Streaming        as B
import qualified Data.ByteString.Streaming.Char8  as Q (lines)
import           Data.Csv                         (EncodeOptions (..),
                                                   HasHeader (..), Quoting (..),
                                                   defaultEncodeOptions)
import           Data.List                        (isSuffixOf)
import qualified Data.Map.Strict                  as Map
import           Data.Typeable
import qualified Data.Vector                      as V
import qualified Data.Vector.Unboxed              as VU
import           GHC.Generics                     (Generic)
import           Options.Generic
import           Streaming
import qualified Streaming.Csv                    as Csv
import qualified Streaming.Prelude                as S

import           Data.Char                        (toUpper)
import           Data.Maybe                       (fromMaybe)
import qualified Data.Text                        as T
import qualified Data.Text.Encoding               as T
import qualified Options.Applicative              as Options
import           Text.Parser.Expression           (Assoc (..), Operator (..),
                                                   buildExpressionParser)
import           Text.Trifecta                    (Parser)
import qualified Text.Trifecta                    as P

data MapOp
  = OpIntersection
  | OpUnion
  | OpDifference
  deriving (Eq, Show, Read, Ord, Generic, ParseField)

type Doc = Int

data MapExpr k a
  = Union (MapExpr k a) (MapExpr k a)
  | Difference (MapExpr k a) (MapExpr k a)
  | Intersection (MapExpr k a) (MapExpr k a)
  | UnionWithAll (MapExpr k a) (MapExpr k a)
  | UnionWithChooseLeft (MapExpr k a) (MapExpr k a)
  | UnionWithChooseRight (MapExpr k a) (MapExpr k a)
  | IntersectionWithAll (MapExpr k a) (MapExpr k a)
  | IntersectionWithChooseLeft (MapExpr k a) (MapExpr k a)
  | IntersectionWithChooseRight (MapExpr k a) (MapExpr k a)
  | Const ![ByteString] (MapExpr k a)
  | NonNullsOf (MapExpr k a)
  | NullsOf (MapExpr k a)
  | Keys (MapExpr k a)
  | Pad {-# UNPACK #-} !Int (MapExpr k a)
  | Columns {-# UNPACK #-} !(VU.Vector Int) (MapExpr k a)
  | DocNum {-# UNPACK #-} !Int
  | Labelled {-# UNPACK #-} !Text (MapExpr k a)
  | Label !Text
  | Result (Map.Map k a)
  | Error
  deriving (Eq, Show, Read, Ord, Generic)

chooseR :: [a] -> [a] -> [a]
chooseR _ a@(_:_) = a
chooseR a _       = a

chooseL :: [a] -> [a] -> [a]
chooseL a@(_:_) _ = a
chooseL _ a       = a

parseMapExpr :: Parser (MapExpr k a)
parseMapExpr =
  buildExpressionParser
  [ [ prefix "nulls" NullsOf
    , prefix "non-nulls" NonNullsOf
    , prefix "keys" Keys
    , Prefix $ do
        _ <- P.textSymbol "const"
        s <- P.brackets (P.sepBy1 P.stringLiteral (P.symbolic ','))
        return (Const (map (T.encodeUtf8 . T.pack) s))
    , Prefix $ do
        _ <-  P.textSymbol "pad"
        amount <- P.natural
        pure (Pad (fromInteger amount))
    , Prefix $ do
        _ <- P.textSymbol "col"
        cols <- (:[]) <$> P.natural <|>
                P.brackets (P.sepBy1 P.natural (P.symbolic ','))
        pure (Columns (VU.fromList (map fromInteger cols)))
    ]
  , [ binary "|*|" IntersectionWithAll AssocLeft
    , binary "|*" IntersectionWithChooseLeft AssocLeft
    , binary "*|" IntersectionWithChooseRight AssocLeft
    , binary "*" Intersection AssocLeft
    ]
  , [ binary "|+|" UnionWithAll AssocRight
    , binary "|+" UnionWithChooseLeft AssocRight
    , binary "+|" UnionWithChooseRight AssocRight
    , binary "+" Union AssocRight
    , binary "-" Difference AssocLeft
    ]
  , [ binary ":" mkLabel AssocRight ]
  ]
  term
  where
    mkLabel (Label t) = Labelled t
    mkLabel _         = const Error

    term =
      P.token (DocNum . fromInteger <$> (P.char '$' *> P.decimal) <|>
               Label <$> P.stringLiteral)
      <|> P.parens parseMapExpr

    prefix name fun = Prefix (fun <$ P.textSymbol name)
    binary name fun = Infix (fun <$ P.textSymbol name)

instance (Typeable k, Typeable a) => ParseField (MapExpr k a) where
  parseField h m c = case m of
    Nothing ->
      Options.argument optTrifectaParser
      $ Options.metavar metavar
      <> foldMap (Options.help . T.unpack) h
    Just name ->
      Options.option optTrifectaParser
      $ Options.metavar metavar
      <> Options.long (T.unpack name)
      <> foldMap (Options.help . T.unpack) h
      <> foldMap Options.short c
    where
      metavar = map toUpper (show (typeOf (undefined :: MapExpr k a)))
      optTrifectaParser = Options.eitherReader $ \t -> fromMaybe
        (Left "unknown error")
        (P.parseString parseMapExpr mempty t ^?
         failing
         (P._Success.re _Right)
         (P._Failure.to show.re _Left))

data Options k a = Options
  { infiles   :: [FilePath] <?> (
      "Files to combine. The first column of each csv will be used as a key. \
      \For non-csv files (\files that don't end in .csv), each line will be \
      \treated as a key.")
  , outfile   :: Maybe FilePath <?> (
      ".csv file to write output to. If omitted, use stdout.")
  , hasHeader :: Maybe Bool <?> "whether to ignore header in csv files"
  , op        :: Maybe MapOp <?> (
      "The operation to use to combine csv files (default: union)")
  , expr      :: Maybe (MapExpr k a) <?> (
      "The expression to use to combine csv files. Reference the inputs with \
      \$N for the Nth document in the \"infiles\".\n\
      \The operations available are:\n\
      \1. Union with the + operator;\n\
      \2. Difference with the - operator;\n\
      \3. Intersection with the * operator;\n\
      \4. Union combining all columns with the +. operator;\n\
      \5 .Intersection combining all columns with the *. operator\n\
      \6. Labels for expressions with the syntax '\"LABEL\": EXPR'.")
  , saveLabels :: Maybe Bool
  , verbose    :: Maybe Bool
  } deriving (Show, Generic)

instance (Typeable k, Typeable a) => ParseRecord (Options k a) where
  parseRecord = parseRecordWithModifiers lispCaseModifiers
    { shortNameModifier = \x -> case x of
        "infiles" -> Just 'i'
        "outfile" -> Just 'o'
        _         -> Nothing
    }

data Row
  = EmptyRow
  | Row !ByteString ![ByteString]

toRow :: [ByteString] -> Either () (ByteString, [ByteString])
toRow (h:hs) = Right (h, hs)
toRow _      = Left ()

-- | select how to decode csv
decode :: Monad m
       => Options k a
       -> B.ByteString m r
       -> Stream (Of (Either String [ByteString])) m ()
decode opts = Csv.decode (if hasHeader' then HasHeader else NoHeader)
  where
    hasHeader' = case unHelpful (hasHeader opts) of
      Just _  -> True
      Nothing -> False

decodeFile
  :: MonadResource m
  => Options k a
  -> FilePath
  -> Stream (Of (Either String [ByteString])) m ()
decodeFile opts fp =
  if isCsv
  then decode opts inputStream
  else S.mapped
       (fmap (S.mapOf (Right . (:[]))) . B.toStrict)
       (Q.lines inputStream)
  where
    isCsv = ".csv" `isSuffixOf` fp || fp == "-"
    inputStream =
      if fp == "-"
      then B.stdin
      else B.readFile fp

-- | select the sink to use for output
output :: MonadResource m => Options k a -> B.ByteString m r -> m r
output opts = case unHelpful (outfile opts) of
  Just out -> B.writeFile out
  Nothing  -> B.stdout

interpret
  :: forall a k e. (Monoid a, Ord k, Ord a, a ~ [e], e ~ ByteString)
  => MapExpr k a
  -> V.Vector (Map.Map k a)
  -> (Map.Map Text (Map.Map k a), Map.Map k a)
interpret me0 v =
  case runState (go me0) Map.empty of
    (Result r, s) -> (getLabels s, r)
    (_, s)        -> (getLabels s, Map.empty)

  where
    takeLabel (Label l) = l
    takeLabel _         = error "takeLabel"

    takeResult (Result a) = a
    takeResult _          = Map.empty

    getLabels = Map.map takeResult . Map.mapKeys takeLabel . Map.filterWithKey (\k _ -> case k of
      Label _ -> True
      _       -> False)

    withResult2 f (Result a) (Result b) = Result (f a b)
    withResult2 _ _           _         = Error

    withResult1 f (Result a) = Result (f a)
    withResult1 _ _          = Error

    ins' k' k =
      k <$ modify' (Map.insert k' k)

    pad i l
      | i > 0 = case l of
          x:xs -> x : pad (i - 1) xs
          []   -> replicate i mempty
      | otherwise = l

    cols cs l =
      VU.foldr
      (\i xs -> fromMaybe mempty (lv V.!? i) : xs)
      []
      cs

      where
        lv = V.fromList l

    rowNull = all (T.null . T.strip . T.decodeUtf8)

    go :: MapExpr k a
       -> State (Map.Map (MapExpr k a) (MapExpr k a)) (MapExpr k a)
    go me = do
      cache <- get
      let ins = ins' me
      case Map.lookup me cache of
        Just x  -> return x
        Nothing -> case me of
          Labelled k a                    -> do
            a' <- go a
            modify' (Map.insert (Label k) a')
            return a'
          Label k                         -> return (Label k)
          DocNum i                        -> return (Result (v V.! (i - 1)))
          Const val a                     -> ins =<< withResult1 (Map.map (const val)) <$> go a
          Union a b                       -> ins =<< withResult2 Map.union <$> go a <*> go b
          Difference a b                  -> ins =<< withResult2 Map.difference <$> go a <*> go b
          Intersection a b                -> ins =<< withResult2 Map.intersection <$> go a <*> go b
          UnionWithAll a b                -> ins =<< withResult2 (Map.unionWith mappend) <$> go a <*> go b
          UnionWithChooseLeft a b         -> ins =<< withResult2 (Map.unionWith chooseL) <$> go a <*> go b
          UnionWithChooseRight a b        -> ins =<< withResult2 (Map.unionWith chooseR) <$> go a <*> go b
          IntersectionWithAll a b         -> ins =<< withResult2 (Map.intersectionWith mappend) <$> go a <*> go b
          IntersectionWithChooseLeft a b  -> ins =<< withResult2 (Map.intersectionWith chooseL) <$> go a <*> go b
          IntersectionWithChooseRight a b -> ins =<< withResult2 (Map.intersectionWith chooseR) <$> go a <*> go b
          NonNullsOf a                    -> ins =<< withResult1 (Map.filter (not . rowNull)) <$> go a
          NullsOf a                       -> ins =<< withResult1 (Map.filter rowNull) <$> go a
          Keys a                          -> ins =<< withResult1 (Map.map (const [])) <$> go a
          Pad i a                         -> ins =<< withResult1 (Map.map (pad (i - 1))) <$> go a
          Columns cs a                    -> ins =<< withResult1 (Map.map (cols cs)) <$> go a
          _                               -> return me

-- | select the combining algorithm
combine :: (Monoid a, Ord k, Ord a, a ~ [e], e ~ ByteString)
        => Options k a
        -> [Map.Map k a]
        -> (Map.Map Text (Map.Map k a), Map.Map k a)
combine opts ms =
  case unHelpful (expr opts) of
    Just expr -> interpret expr (V.fromList ms)
    Nothing -> (Map.empty, case unHelpful (op opts) of
      Just OpIntersection -> foldr1 Map.intersection ms
      Just OpDifference   -> foldr1 Map.difference ms
      _union              -> Map.unions ms)

encodeOpts :: EncodeOptions
encodeOpts = defaultEncodeOptions
  { encQuoting = QuoteAll
  }

main :: IO ()
main = do
  opts@Options{..} <- getRecord "Map-like operations on csv files"
  when (verbose == Just True) (print opts)
  runResourceT $ mapM
    -- write infiles as maps
    (fmap (Map.fromList . S.fst')
     . S.toList
     . S.effects
     . S.partitionEithers
     . S.map (either (const (Left ())) toRow)
     . decodeFile opts)
    (unHelpful infiles) >>= \o ->
    -- write output
    let (labelled, r) = combine opts o
        writeCsv opts' = output opts'
                         . Csv.encodeWith encodeOpts
                         . mapM_ (S.yield . uncurry (:))
                         . Map.toList
    in do
      writeCsv opts r
      when (saveLabels == Just True) $ iforM_ labelled $ \name -> writeCsv opts
        { outfile = Helpful (Just (T.unpack name ++ ".csv"))
        }
